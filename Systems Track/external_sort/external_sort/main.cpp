#include "includes.h"
#include "externalSort.h"

int main(void) {
	printf("Enter input file : "); 
	char* inputfileName = (char*)malloc(20 * sizeof(char)); 
	scanf("%s", inputfileName); 
	printf("Enter output file name : "); 
	char* outputfileName = (char*)malloc(20 * sizeof(char)); 
	scanf("%s", outputfileName); 
	int capacity; 
	printf("Enter RAM capacity : "); 
	scanf("%d", &capacity); 
	externalSort(inputfileName, outputfileName, capacity); 
	return 0; 
}