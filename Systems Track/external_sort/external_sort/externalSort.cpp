#include "includes.h"
#include "externalSort.h"

int findFileSize(FILE* file) {
	int size = 0; 
	int temp; 
	while (fscanf_s(file, "%d", &temp) == 1)
		size++; 
	rewind(file); 
	return size; 
}

int getMinIndex(int* arr, int* shouldConsider, int size) {
	int min = 10000000; 
	int minIndex; 
	for (int i = 0; i < size; i++) {
		if (shouldConsider[i] && arr[i] < min) {
			min = arr[i]; 
			minIndex = i; 
		}
	}
	return minIndex; 
}

int isEndOfAllFiles(int* arr, int size) {
	for (int i = 0; i < size; i++)
		if (arr[i] == 1)
			return 0; 
	return 1; 
}

void externalSort(char* inputfileName, char* outputfileName, int capacity) {
	FILE* inputfile = fopen(inputfileName, "r"); 
	FILE* outputfile = fopen(outputfileName, "w"); 
	int fileSize = findFileSize(inputfile); 
	int noOfIterations = fileSize / capacity; 
	int iteration = 0; 
	FILE** tempfiles = (FILE * *)malloc(noOfIterations * sizeof(FILE*)); 
	FILE* tempfile = (FILE*)malloc(sizeof(FILE)); 
	while (!feof(inputfile)) {
		int pos = 0; 
		int* arr = (int*)malloc(capacity * sizeof(int)); 
		while (pos < capacity && fscanf(inputfile, "%d", &arr[pos++]) == 1); 
		std::sort(arr, arr + pos); 
		char tempfileName[20] = "temp"; 
		char* iterName = (char*)malloc(10 * sizeof(char)); 
		_itoa(iteration, iterName, 10); 
		strcat(tempfileName, iterName); 
		strcat(tempfileName, ".txt"); 
		tempfile = fopen(tempfileName, "w+"); 
		for (int i = 0; i < pos; i++)
			fprintf(tempfile, "%d\n", arr[i]); 
		rewind(tempfile); 
		tempfiles[iteration] = tempfile; 
		iteration++; 
		tempfile = (FILE*)malloc(sizeof(FILE)); 
	}

	iteration = 0; 
	int* comparingValues = (int*)malloc(noOfIterations * sizeof(int)); 
	for (iteration = 0; iteration < noOfIterations; iteration++) 
		fscanf(tempfiles[iteration], "%d", &comparingValues[iteration]); 
	int* shouldConsider = (int*)malloc(noOfIterations * sizeof(int)); 
	for (int i = 0; i < noOfIterations; i++)
		shouldConsider[i] = 1; 

	while (!isEndOfAllFiles(shouldConsider, noOfIterations)) {
		int index = getMinIndex(comparingValues, shouldConsider, noOfIterations); 
		fprintf(outputfile, "%d\n", comparingValues[index]); 
		int value;
		if ((feof(tempfiles[index])) || fscanf(tempfiles[index], "%d", &value) == 0)
			shouldConsider[index] = 0; 
		else
			comparingValues[index] = value; 
	}
	for (int i = 0; i < noOfIterations; i++)
		fclose(tempfiles[i]); 
	fclose(outputfile); 
}