#include "includes.h"
#include "block.h"
#include "meta.h"
#include "operations.h"

char* get_next_word(char** line) {
	if (*line == NULL)
		return NULL; 
	while (**line == ' ') {
		*(*line)++; 
	}
	char* ans = (char*)malloc(10 * sizeof(char)); 
	int pos = 0; 
	while (**line != ' ' & **line != '\n' && **line != '\0') {
		ans[pos] = **line; 
		pos++; 
		*(*line)++; 
	}
	ans[pos] = '\0'; 
	return ans; 
}

int main()
{
	char* string = (char*)malloc(100* sizeof(char));
	char* command = (char*)malloc(100 * sizeof(char));
	char* source = (char*)malloc(100 * sizeof(char));
	char* destination = (char*)malloc(100 * sizeof(char));
	while (1)
	{
		int cindx = 0, source_indx = 0, dest_indx = 0, i = 0;
		printf(">");
		fgets(string, 300, stdin);
		char* command = get_next_word(&string); 
		if (strcmp(command, "format") == 0)
		{
			format();
		}
		else if (strcmp(command, "listfiles") == 0)
		{
			list();
		}
		else if (strcmp(command, "copytodisk") == 0)
		{
			copy_to_disk(source, destination);
		}
		else if (strcmp(command, "copyfromdisk") == 0)
		{
			copy_from_disk(source, destination);
		}
		else
			printf("invalid command");
	}
}