//0x444e524d
#pragma once
#define _HARDDISK "hard_disk.hdd"
#define _BLOCK_SIZE 16384
#define _CRT_SECURE_NO_WARNINGS
#define _NO_OF_META_DATA_BLOCKS 400
#define _MAX_NO_OF_FILES 32
#include <stdio.h>
#include <stdlib.h>
#include <string.h>