#include "includes.h"
#include "operations.h"
#include "block.h"
#include "meta.h"

int can_fit(int size) {
	meta_data meta; 
	char buffer[_BLOCK_SIZE]; 
	read_block(0, buffer); 
	memcpy(&meta, buffer, sizeof(meta)); 
	if (meta.magic_number != 0x444e524d) {
		printf("Hard disk corrupted, please format it before using\n"); 
		return 0; 
	}
	return size < meta.available_free_space; 
}

int find_free_blocks(int size) {
	meta_data meta; 
	char buffer[_BLOCK_SIZE]; 
	read_block(0, buffer); 
	memcpy(&meta, buffer, sizeof(meta)); 
	int required_blocks = size; 
	int j; 
	for (int i = 0; i < 6000; i++) {
		j = i; 
		required_blocks = size; 
		if (meta.free_blocks[i]) {
			while (required_blocks != 0 && meta.free_blocks[j] == 1) {
				j++; 
				required_blocks--; 
			}
			if (required_blocks == 0)
				return i + _NO_OF_META_DATA_BLOCKS; 
		}
	}
	printf("Space not available :(\n"); 
	return -1; 
}

int file_exists(char* file) {
	char buffer[_BLOCK_SIZE]; 
	read_block(0, buffer); 
	meta_data meta; 
	memcpy(&meta, buffer, sizeof(meta)); 
	for (int i = 0; i < meta.no_of_files; i++)
		if (strcmp(file, meta.files[i].filename) == 0)
			return i; 
	return -1; 
}

unsigned int get_size(FILE* file) {
	fseek(file, 0, SEEK_END); 
	return ftell(file); 
}

void copy_to_disk(char* source, char* destination) {
	FILE* sourceptr = fopen(source, "rb"); 
	if (sourceptr == NULL) {
		printf("Cannot read the source file :(\n"); 
		return; 
	}
	int source_size = get_size(sourceptr); 
	if (can_fit(source_size) == 0) {
		printf("Space not available in the harddisk :(\n"); 
		return; 
	}
	int blocks_required = source_size / _BLOCK_SIZE; 
	if (source_size % _BLOCK_SIZE != 0)
		blocks_required += 1; 
	int start_block = find_free_blocks(blocks_required); 
	meta_data meta; 
	char buffer[_BLOCK_SIZE]; 
	read_block(0, &buffer); 
	memcpy(&meta, buffer, sizeof(meta)); 
	if (meta.magic_number != 0x444e524d) {
		printf("Hard disk corrupted please format before using it\n"); 
		return; 
	}
	file_data file; 
	strcpy(file.filename, source); 
	file.start_block = start_block; 
	file.file_length = source_size; 
	file.no_of_blocks = blocks_required; 
	meta.files[meta.no_of_files] = file; 
	for (int i = start_block - _NO_OF_META_DATA_BLOCKS; i < start_block + blocks_required - _NO_OF_META_DATA_BLOCKS; i++)
		meta.free_blocks[i] = 0; 
	meta.available_free_space -= blocks_required * _BLOCK_SIZE; 
	meta.no_of_files += 1; 
	memcpy(buffer, &meta, sizeof(meta));
	write_block(0, buffer);
	rewind(sourceptr); 
	int i = 0; 
	for (i = start_block; i < start_block + blocks_required - 1; i++) {
		fread(buffer, 1, _BLOCK_SIZE, sourceptr); 
		write_block(i, buffer); 
	}
	int remaining_bytes = source_size - ((blocks_required - 1) * _BLOCK_SIZE);
	fread(buffer, 1, remaining_bytes, sourceptr); 
	write_block(i, buffer);
	fclose(sourceptr); 
}

void copy_from_disk(char* source, char* destination) {
	char buffer[_BLOCK_SIZE]; 
	read_block(0, buffer); 
	meta_data meta; 
	memcpy(&meta, buffer, sizeof(meta)); 
	int file_index = file_exists(source); 
	if (file_index == -1) {
		printf("File not found\n"); 
		return; 
	}
	FILE* destinationptr = fopen(destination, "wb"); 
	if (destinationptr == NULL) {
		printf("Cannot open the destination file\n"); 
		return; 
	}
	int start_block = meta.files[file_index].start_block; 
	int file_length = meta.files[file_index].file_length; 
	int no_of_blocks = meta.files[file_index].no_of_blocks; 
	int i; 
	for (i = start_block; i < start_block + no_of_blocks - 1; i++) {
		read_block(i, buffer); 
		fwrite(buffer, 1, _BLOCK_SIZE, destinationptr); 
	}
	read_block(i, buffer); 
	int remaining_bytes = file_length - ((no_of_blocks -1) * _BLOCK_SIZE); 
	fwrite(buffer, 1, remaining_bytes, destinationptr); 
	fclose(destinationptr); 
}

void delete_file(char* file_name) {
	char buffer[_BLOCK_SIZE];
	read_block(0, buffer);
	meta_data meta;
	memcpy(&meta, buffer, sizeof(meta));
	int file_index = file_exists(file_name);
	if (file_index == -1) {
		printf("File not found\n");
		return;
	}
	int start_block = meta.files[file_index].start_block;
	int file_length = meta.files[file_index].file_length;
	int no_of_blocks = meta.files[file_index].no_of_blocks;
	int i;
	for (i = start_block - _NO_OF_META_DATA_BLOCKS; i < start_block + no_of_blocks - _NO_OF_META_DATA_BLOCKS; i++) {
		meta.free_blocks[i] = 0; 
	}
	memcpy(buffer, &meta, sizeof(meta));
	write_block(0, buffer); 
}

void list() {
	meta_data meta; 
	char buffer[_BLOCK_SIZE]; 
	read_block(0, buffer); 
	memcpy(&meta, buffer, sizeof(meta)); 
	for (int i = 0; i < meta.no_of_files; i++)
		printf("%s\n", meta.files[i].filename); 
}

void format() {
	meta_data meta; 
	char buffer[_BLOCK_SIZE]; 
	read_block(0, buffer); 
	memcpy(&meta, buffer, sizeof(meta)); 
	char empty[32]; 
	for (int i = 0; i < 32; i++)
		empty[i] = 0; 
	for (int i = 0; i < 32; i++)
		memcpy(&meta.files[i], empty, sizeof(file_data)); 
	meta.no_of_files = 0; 
	meta.magic_number = 0x444e524d; 
	for (int i = 0; i < 6000; i++)
		meta.free_blocks[i] = 1;
	meta.available_free_space = 6000 * _BLOCK_SIZE; 
	memcpy(buffer, &meta, sizeof(meta)); 
	write_block(0, buffer); 
}
