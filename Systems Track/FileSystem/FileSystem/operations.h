#include "includes.h"

unsigned int get_size(FILE*); 
void copy_to_disk(char*, char*); 
void copy_from_disk(char*, char*); 
void list(); 
void print_debug_info(); 
void format(); 
void delete_file(); 