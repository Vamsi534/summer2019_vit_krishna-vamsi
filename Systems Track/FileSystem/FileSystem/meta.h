#pragma once
#include "includes.h"

typedef struct File_Data{
	char filename[20]; 
	unsigned int start_block; 
	unsigned int no_of_blocks; 
	unsigned int file_length; 
} file_data;

typedef struct meta_data {
	unsigned int magic_number; 
	unsigned int no_of_files; 
	file_data files[_MAX_NO_OF_FILES]; 
	unsigned int available_free_space; 
	char free_blocks[6000]; 
} meta_data;
