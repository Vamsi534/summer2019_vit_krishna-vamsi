#include "includes.h"
#include "block.h"

int write_block(int block_no, void* base_address) {
	FILE* harddisk = fopen(_HARDDISK, "rb+"); 
	if (harddisk == NULL) {
		return -1; 
	}
	fseek(harddisk, (block_no) * _BLOCK_SIZE, SEEK_SET); 
	int args_written = fwrite(base_address, 1, _BLOCK_SIZE, harddisk);
	fclose(harddisk); 
	return args_written; 
}

int read_block(int block_no, void* base_address) {
	FILE* harddisk = fopen(_HARDDISK, "rb+"); 
	if (harddisk == NULL) {
		return -1; 
	}
	fseek(harddisk, (block_no) * _BLOCK_SIZE, SEEK_SET); 
	int args_read = fread(base_address, 1, _BLOCK_SIZE, harddisk); 
	fclose(harddisk); 
	return args_read; 
}