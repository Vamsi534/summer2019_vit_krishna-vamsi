#include "includes.h"

int recPow(int a, int n) {
	if (n == 0)
		return 1;
	if (n == 1)
		return a; 
	int t = (recPow(a, n / 2)) % 10000007; 
	t = ((t % 10000007) * (t % 10000007)) % 10000007; 
	if (n % 2 == 0)
		return t; 
	return ((t % 10000007) * (a % 10000007)); 
}

int recPowmain(void) {
	int a, n; 
	printf("Enter the value of a : "); 
	scanf("%d", &a); 
	printf("Enter the value of n : "); 
	scanf("%d", &n); 
	printf("%d", recPow(a, n)); 
	return 0; 
}