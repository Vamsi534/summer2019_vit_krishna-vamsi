typedef struct DLLNode {
	int data; 
	struct DLLNode* next; 
	struct DLLNode* prev; 
}DLLNode;

DLLNode* createNode(int); 
DLLNode* createList(int*, int); 
DLLNode* mergeSort(DLLNode*); 
DLLNode* merge(DLLNode*, DLLNode*); 
void display(DLLNode*); 