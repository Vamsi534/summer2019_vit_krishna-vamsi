#include "includes.h"

long arraySumRec(int* arr, int size) {
	if (size == 1)
		return arr[0]; 
	return arr[0] + arraySumRec(arr + 1, size - 1); 
}

int arraySummain(void) {
	int size; 
	printf("Enter the size of the array : "); 
	scanf("%d", &size); 
	int* arr = (int*)malloc(size * sizeof(int)); 
	printf("Enter the array elements : \n"); 
	for (int i = 0; i < size; i++)
		scanf("%d", &arr[i]); 
	printf("%ld\n", arraySumRec(arr, size)); 
	return 0; 
}