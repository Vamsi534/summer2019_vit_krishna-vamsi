#include "includes.h"

void stairCase(int n, int k, int pos, int* count) {
	if (pos > n)
		return; 
	if (pos == n) {
		(*count)++; 
		return; 
	}
	for (int i = 1; i <= k; i++) {
		stairCase(n, k, pos + i, count); 
	}
}

int stairCasemain(void) {
	int n, k; 
	printf("Enter number of stairs : "); 
	scanf("%d", &n); 
	printf("Enter the value of k : "); 
	scanf("%d", &k); 
	int count = 0; 
	stairCase(n, k, 0, &count); 
	if (count)
		printf("Number of steps : %d\n", count);
	else
		printf("No possible ways :(\n"); 
	return 0; 
}