#include "includes.h"

void print(int* arr, int n) {
	for (int i = 0; i < n; i++)
		printf("%d", arr[i]); 
	printf("\n"); 
}

void permutation(int* arr, int n, int* ans, int pos, int* selected) {
	if (pos == n) {
		print(ans, n); 
		return; 
	}
	for (int i = 0; i < n; i++) {
		if (selected[i] == 0) {
			ans[pos] = arr[i];
			selected[i] = 1; 
			permutation(arr, n, ans, pos + 1, selected); 
			selected[i] = 0; 
		}
	}
}

int permutationsmain(void) {
	int n; 
	printf("Enter the size of the array : "); 
	scanf("%d", &n); 
	int* arr = (int*)malloc(n * sizeof(int)); 
	printf("Enter the array elements : \n"); 
	for (int i = 0; i < n; i++)
		scanf("%d", &arr[i]); 
	int* selected = (int*)malloc(n * sizeof(int)); 
	for (int i = 0; i < n; i++)
		selected[i] = 0; 
	int* ans = (int*)malloc(n * sizeof(int)); 
	permutation(arr, n, ans, 0, selected); 
	return 0; 
}