#include "includes.h"

long fib(long* arr, int n) {
	if (n < 0)
		return -1; 
	else if (n == 0 || n == 1)
		return n; 
	if (arr[n] != -1)
		return arr[n]; 
	arr[n] = fib(arr, n - 1) + fib(arr, n - 2); 
	return arr[n]; 
}

int fibmain(void) {
	int n; 
	printf("Enter a number : "); 
	scanf("%d", &n); 
	long* arr = (long*)malloc((n + 1) * sizeof(long)); 
	for (int i = 0; i <= n; i++)
		arr[i] = -1; 
	printf("%ld", fib(arr, n)); 
	return 0; 
}