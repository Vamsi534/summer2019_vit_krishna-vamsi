#include "includes.h"

void print(char* arr, int n) {
	int start = 0; 
	while (start < n && arr[start] == '0')
		start++; 
	if (start == n - 1) {
		printf("0\t"); 
		return; 
	}
	for (int i = start; i < n && arr[i] != '\0'; i++)
		printf("%c", arr[i]); 
	printf("\t"); 
}

void printRev(char* arr, int n) {
	char rev[] = { '0', '1', '2', 'E', 'h', '5', '9', 'L', '8', '6' }; 
	int start = 0;
	while (start < n && arr[start] == '0')
		start++;
	if (start == n - 1) {
		printf("0\t");
		return;
	}
	for (int i = start; i < n && arr[i] != '\0'; i++)
		printf("%c", rev[arr[i] - '0']);
	printf("\t");
}

void printNumbers(int n, char* num, int pos) {
	if (n == 1)
		return; 
	if (pos == n - 1) {
		print(num, n); 
		printRev(num, n); 
		printf("\n"); 
		return; 
	}
	for (int i = 0; i < 10; i++) {
		num[pos] = '0' + i; 
		printNumbers(n, num, pos + 1); 
	}
	num[pos] = '\0'; 
}

int printNumbersmain(void) {
	int n; 
	printf("Enter a number : "); 
	scanf("%d", &n); 
	char* num = (char*)malloc(n * sizeof(char)); 
	for (int i = 0; i < n; i++)
		num[i] = '\0'; 
	printNumbers(n, num, 0); 
	return 0; 
}