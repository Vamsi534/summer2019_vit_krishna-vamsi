#include "DLL.h"
#include "includes.h"

DLLNode* createNode(int data) {
	DLLNode* newNode = (DLLNode*)malloc(sizeof(DLLNode)); 
	if (newNode == NULL) {
		perror("Error creating new node\n"); 
		return NULL; 
	}
	else {
		newNode->data = data;
		newNode->next = NULL;
		newNode->prev = NULL;
		return newNode;
	}
}

DLLNode* createList(int* arr, int size) {
	if (size == 0)
		return NULL; 
	DLLNode* head = createNode(arr[0]); 
	DLLNode* temp = head;
	for (int i = 1; i < size; i++) {
		DLLNode* newNode = createNode(arr[i]); 
		temp->next = newNode; 
		newNode->prev = temp; 
		temp = temp->next; 
	}
	return head; 
}

void display(DLLNode* head) {
	if (head == NULL)
		return; 
	DLLNode* temp = head; 
	while (temp->next) {
		printf("%d <-> ", temp->data); 
		temp = temp->next; 
	}
	printf("%d\n", temp->data); 
}

//DLLNode* mergeSort(DLLNode* start, DLLNode* end) {
//	if (start->next == end)
//		return;
//	DLLNode* slowptr = start; 
//	DLLNode* fastptr = start; 
//	while (fastptr != end) {
//		slowptr = slowptr->next; 
//		fastptr = fastptr->next->next; 
//	}
//	slowptr->next = NULL; 
//	fastptr->prev = NULL; 
//	head1 = mergeSort(head1, slowptr); 
//	head2 = mergeSort(slowptr->next, head2); 
//	head1 = merge(head1, head2); 
//	return head1; 
//}

DLLNode* merge(DLLNode* head1, DLLNode* head2) {
	DLLNode* temp1 = head1; 
	DLLNode* temp2 = head2; 
	DLLNode* temp = NULL; 
	while (temp1 && temp2) {
		if (temp1->data < temp2->data) {
			if (temp == NULL) {
				temp = temp1; 
				head1 = temp; 
			}
			temp->next = temp1; 
			temp1->prev = temp; 
		}
		else {
			if (temp == NULL) {
				temp = temp2;
				head1 = temp; 
			}
			temp->next = temp2;
			temp2->prev = temp;
		}
		temp1 = temp1->next; 
		temp2 = temp2->next; 
	}

	while (temp1) {
		temp->next = temp1; 
		temp1->prev = temp; 
		temp1 = temp1->next; 
	}

	while (temp2) {
		temp->next = temp2; 
		temp2->prev = temp; 
		temp2 = temp2->next; 
	}
	return head1; 
}