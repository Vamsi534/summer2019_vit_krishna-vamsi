#pragma once

void sortByCountingOnes(int*, int);
void sort(int*, int);
void sortBySwapping(int*, int);
void display(int*, int); 
void swap(int*, int*); 
void twoColorSortMain(void); 