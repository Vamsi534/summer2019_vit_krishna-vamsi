#include "twoColorSort.h"
#include "includes.h"

void sortByCountingOnes(int* arr, int n) {
	int count = 0; 
	for (int i = 0; i < n; i++)
		count += arr[i]; 
	printf("Number of zeros : %d\n", n - count); 
	for (int i = 0; i < n - count; i++)
		arr[i] = 0; 
	for (int i = n - count; i < n; i++)
		arr[i] = 1; 
}

void swap(int* a, int* b) {
	int temp = *a; 
	*a = *b; 
	*b = temp; 
}

void sort(int* arr, int n) {
	for (int i = 0; i < n; i++) {
		for (int j = i + 1; j < n; j++) {
			if (arr[i] > arr[j])
				swap(&arr[i], &arr[j]); 
		}
	}
}

void display(int* arr, int size) {
	for (int i = 0; i < size; i++)
		printf("%d ", arr[i]); 
	printf("\n"); 
}

void sortBySwapping(int* arr, int size) {
	int onesPos = 0; 
	int zerosPos = size - 1; 
	while (1) {
		while (arr[onesPos] != 1)
			onesPos++; 
		while (arr[zerosPos] != 0)
			zerosPos--; 
		if (onesPos > zerosPos)
			break; 
		swap(&arr[onesPos], &arr[zerosPos]); 
	}
}

void twoColorSortMain(void) {
	int n;
	printf("Enter the size of the array : ");
	scanf("%d", &n);
	int* arr = (int*)malloc(n * sizeof(int));
	printf("Enter the array elements : ");
	for (int i = 0; i < n; i++)
		scanf("%d", &arr[i]);
	sortByCountingOnes(arr, n);
	printf("After sorting using counting ones method : \n");
	display(arr, n);
	sort(arr, n); 
	printf("After sorting using bubble sort method : \n");
	display(arr, n);
	sortBySwapping(arr, n); 
	printf("After sorting using swapping method : \n");
	display(arr, n);
}