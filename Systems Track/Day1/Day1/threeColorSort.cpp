#pragma once
#include "includes.h"
#include "threeColorSort.h"
#include "twoColorSort.h"

void sortThreeColors(int* arr, int size) {
	int nonZeroPos = 0, zeroPos = size - 1; 
	while (1) {
		while (arr[nonZeroPos] == 0)
			nonZeroPos++; 
		while (arr[zeroPos] != 0)
			zeroPos--; 
		if (nonZeroPos > zeroPos)
			break; 
		swap(&arr[nonZeroPos], &arr[zeroPos]); 
	}
	int twoPos = 0, onePos = size - 1; 
	while (1) {
		while (arr[twoPos] != 2)
			twoPos++; 
		while (arr[onePos] != 1)
			onePos--; 
		if (twoPos > onePos)
			break; 
		swap(&arr[onePos], &arr[twoPos]); 
	}
}

void threeColorSortMain(void) {
	int n; 
	printf("Enter the size of the array : "); 
	scanf("%d", &n); 
	int* arr = (int*)malloc(n * sizeof(int)); 
	printf("Enter the array elements : "); 
	for (int i = 0; i < n; i++)
		scanf("%d", &arr[i]); 
	sortThreeColors(arr, n); 
	printf("After sorting using threecolorsort : \n"); 
	display(arr, n); 
}