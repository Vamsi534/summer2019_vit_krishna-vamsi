#pragma once
#include "includes.h"
#include "list.h"

node* createNode(int data) {
	node* newNode = (node*)malloc(sizeof(node)); 
	if (newNode == NULL) {
		perror("Cannot create new node\n"); 
		return NULL; 
	}
	newNode->data = data; 
	newNode->next = NULL; 
	return newNode; 
}

node* createList(int* arr, int size) {
	if (size == 0)
		return NULL; 
	node* head = createNode(arr[0]); 
	node* temp = head; 
	for (int i = 1; i < size; i++) {
		temp->next = createNode(arr[i]); 
		temp = temp->next; 
	}
	return head; 
}

node* reverse(node* head) {
	if (head == NULL || head->next == NULL)
		return head;
	node* prev = NULL; 
	node* temp = head; 
	node* next = NULL; 
	while (!temp) {
		next = temp->next; 
		temp->next = prev; 
		prev = temp; 
		temp = next; 
	}
	return prev; 
}

int detectLoop(node* head) {
	if (head == NULL || head->next == NULL)
		return 0; 
	node* slowptr = head; 
	node* fastptr = head->next->next; 
	while (fastptr && slowptr != fastptr) {
		slowptr = slowptr->next; 
		fastptr = fastptr->next->next; 
	}
	if (fastptr == NULL)
		return 0;
	return 1; 
}

node* addLists(node* num1, node* num2, int* carry, int pos, int diff) {
	if (num1 == NULL)
		return NULL; 
	node* ans = NULL; 
	if (pos < diff) {
		node* remaining = addLists(num1->next, num2, carry, pos + 1, diff);
		ans = createNode((num1->data + *carry) % 10); 
		*carry = (num1->data + *carry) / 10; 
		ans->next = remaining; 
		return ans; 
	}
	node* remaining = addLists(num1->next, num2->next, carry, pos + 1, diff);
	ans = createNode((num1->data + num2->data + *carry) % 10); 
	*carry = (num1->data + num2->data + *carry) / 10; 
	ans->next = remaining; 
	return ans; 
}

node* addTwoNumbersFromLinkedList(node* num1, node* num2) {
	int len1 = length(num1); 
	int len2 = length(num2); 
	if (len2 > len1) {
		node* temp = num1; 
		num1 = num2; 
		num2 = temp; 
		int tempInt = len1; 
		len1 = len2; 
		len2 = tempInt; 
	}
	int carry = 0; 
	node* remaining = addLists(num1, num2, &carry, 0, len1 - len2); 
	if (carry) {
		node* ans = createNode(carry); 
		ans->next = remaining; 
		return ans; 
	}
	return remaining; 
}

void displayLinkedList(node* head) {
	if (head == NULL)
		return; 
	node* temp = head; 
	while (temp->next) {
		printf("%d -> ", temp->data); 
		temp = temp->next; 
	}
	printf("%d\n", temp->data); 
}

int length(node* head) {
	node* temp = head; 
	int len = 0; 
	while (temp)
		len++, temp = temp->next; 
	return len; 
}

void addNumbersMain(void) {
	int len1;
	printf("Enter size of the first list: ");
	scanf("%d", &len1);
	int* arr1 = (int*)malloc(len1 * sizeof(int));
	printf("Enter first list elements : \n");
	for (int i = 0; i < len1; i++)
		scanf("%d", &arr1[i]);
	node* num1 = createList(arr1, len1);
	int len2;
	printf("Enter size of the second list: ");
	scanf("%d", &len2);
	int* arr2 = (int*)malloc(len2 * sizeof(int));
	printf("Enter first list elements : \n");
	for (int i = 0; i < len2; i++)
		scanf("%d", &arr2[i]);
	node* num2 = createList(arr2, len2);
	node* sum = addTwoNumbersFromLinkedList(num1, num2);
	displayLinkedList(sum);
}

void detectMerge(node* head1, node* head2) {
	int len1 = length(head1); 
	int len2 = length(head2); 
	if (len2 > len1) {
		node* temp = head1; 
		head1 = head2; 
		head2 = temp; 
		int tempInt = len1; 
		len1 = len2; 
		len2 = tempInt; 
	}
	int count = 0; 
	node* temp1 = head1; 
	node* temp2 = head2; 
	for (int i = 0; i < len1 - len2; i++) 
		temp1 = temp1->next; 
	while (temp1 && temp2) {
		if (temp1 == temp2) {
			printf("The common node is : %d\n", temp1->data); 
			return; 
		}
		temp1 = temp1->next; 
		temp2 = temp2->next; 
	}
	printf("Given lists do not merge at any node\n"); 
}

void detectMergeMain() {
	node* newNode;
	node* head1 = (node*) malloc(sizeof(node));
	head1->data = 10;

	node* head2 = (node*) malloc(sizeof(node));
	head2->data = 3;

	newNode = (node*) malloc(sizeof(node));
	newNode->data = 6;
	head2->next = newNode;

	newNode = (node*) malloc(sizeof(node));
	newNode->data = 9;
	head2->next->next = newNode;

	newNode = (node*) malloc(sizeof(node));
	newNode->data = 15;
	head1->next = newNode;
	head2->next->next->next = newNode;

	newNode = (node*) malloc(sizeof(node));
	newNode->data = 30;
	head1->next->next = newNode;

	head1->next->next->next = NULL;

	detectMerge(head1, head2); 
}

void detectLoopMain(void) {
	int arr[] = { 1, 2, 3, 4, 5 }; 
	node* head = createList(arr, 5); 
	node* temp = head; 
	while (temp->next)
		temp = temp->next; 
	//temp->next = head->next->next; 
	if (detectLoop(head))
		printf("Given list contains loop\n");
	else
		printf("Given list does not contain loop\n"); 
}

node* reverseEveryKNodes(node* head) {
	return NULL; 
}