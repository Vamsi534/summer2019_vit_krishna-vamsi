#include "includes.h"
#include "spiralMatrix.h"

void spiralIteration(int** matrix, int rows, int cols) {
	int top = 0, bottom = rows - 1, left = 0, right = cols - 1; 
	while (top <= bottom && left <= right) {
		for (int i = left; i <= right; i++)
			printf("%d ", matrix[top][i]); 
		top++; 
		for (int i = top; i <= bottom; i++)
			printf("%d ", matrix[i][right]); 
		right--; 
		for (int i = right; i >= left; i--)
			printf("%d ", matrix[bottom][i]); 
		bottom--; 
		for (int i = bottom; i >= top; i--)
			printf("%d ", matrix[i][left]); 
		left++; 
	}
	printf("\n"); 
}

void spiralRecursion(int** matrix, int top, int bottom, int left, int right) {
	if (top > bottom || left > right)
		return; 
	for (int i = left; i <= right; i++)
		printf("%d ", matrix[top][i]);
	top++;
	for (int i = top; i <= bottom; i++)
		printf("%d ", matrix[i][right]);
	right--;
	for (int i = right; i >= left; i--)
		printf("%d ", matrix[bottom][i]);
	bottom--;
	for (int i = bottom; i >= top; i--)
		printf("%d ", matrix[i][left]);
	left++;
	if (top <= bottom && left <= right)
		spiralRecursion(matrix, top, bottom, left, right); 
}

void spiralMatrixMain() {
	int rows, cols; 
	printf("Enter the size of the matrix : "); 
	scanf("%d %d", &rows, &cols); 
	int** matrix = (int**)malloc(rows * sizeof(int*)); 
	for (int i = 0; i < rows; i++)
		matrix[i] = (int*)malloc(cols * sizeof(int)); 
	printf("Enter the elements of the matrix : \n"); 
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			scanf("%d", &matrix[i][j]); 
	printf("Spiral traversal of the given matrix iteratively is : \n"); 
	spiralIteration(matrix, rows, cols); 
	printf("Spiral traversal of the given matrix recursively is : \n"); 
	spiralRecursion(matrix, 0, rows - 1, 0, cols - 1); 
}