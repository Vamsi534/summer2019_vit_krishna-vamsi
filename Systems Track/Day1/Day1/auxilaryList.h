#include "includes.h"

typedef struct auxilaryNode {
	int data; 
	struct auxilaryNode* next; 
	struct auxilaryNode* random;
} auxilaryNode;

auxilaryNode* createAuxNode(int); 
auxilaryNode* clone(auxilaryNode*); 
void printAuxList(auxilaryNode*); 
void auxilaryListMain(void); 