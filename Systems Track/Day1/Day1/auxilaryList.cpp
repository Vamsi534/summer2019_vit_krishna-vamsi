#include "auxilaryList.h"

auxilaryNode* createAuxNode(int data) {
	auxilaryNode* newNode = (auxilaryNode*)malloc(sizeof(auxilaryNode));
	newNode->data = data;
	newNode->next = NULL;
	newNode->random = NULL;
	return newNode;
}

auxilaryNode* clone(auxilaryNode* start) {
	auxilaryNode* curr = start, * temp;
	while (curr) {
		temp = curr->next;
		curr->next = createAuxNode(curr->data);
		curr->next->next = temp;
		curr = temp;
	}
	curr = start;
	while (curr) {
		if (curr->next)
			curr->next->random = curr->random ? curr->random->next : curr->random;
		curr = curr->next ? curr->next->next : curr->next;
	}
	auxilaryNode* original = start, * copy = start->next;
	temp = copy;
	while (original && copy) {
		original->next =
			original->next ? original->next->next : original->next;

		copy->next = copy->next ? copy->next->next : copy->next;
		original = original->next;
		copy = copy->next;
	}
	return temp;
}

void printAuxList(auxilaryNode* start)
{
	auxilaryNode* ptr = start;
	while (ptr)
	{
		printf("Data = %d, Random  = %d\n", ptr->data, ptr->random->data);
		ptr = ptr->next;
	}
}

void auxilaryListMain(void) {
	auxilaryNode* start = createAuxNode(1);
	start->next = createAuxNode(2);
	start->next->next = createAuxNode(3);
	start->next->next->next = createAuxNode(4);
	start->next->next->next->next = createAuxNode(5);

	start->random = start->next->next;
	start->next->random = start;
	start->next->next->random = start->next->next->next->next;
	start->next->next->next->random = start->next->next->next->next;
	start->next->next->next->next->random = start->next;
	printf("Original list : \n");
	printAuxList(start);
	printf("Cloned list : \n");
	auxilaryNode* cloned = clone(start);
	printAuxList(cloned);
}