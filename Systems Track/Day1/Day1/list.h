#pragma once
#include "includes.h"

typedef struct Node {
	int data; 
	struct Node* next; 
} node;

node* createNode(int); 
node* createList(int*, int); 
int detectLoop(node*); 
node* reverse(node*); 
void detectLoopMain(); 
void detectMerge(node*, node*); 
void detectMergeMain(); 
node* addTwoNumbersFromLinkedList(node*, node*); 
int length(node*); 
void displayLinkedList(node*); 
void addNumbersMain(void); 
node* reverseEveryKNodes(node*); 
void reverseEveryKNodesMain(); 