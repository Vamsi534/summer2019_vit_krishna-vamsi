import openpyxl
import click
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'classproject.settings')
import django
django.setup()

from onlineapp.models import College, Student, Mocktest1


def extractDBname(string):
    return string.split('_')[2]


@click.group()
def import_data():
    """Import data from files to the database"""


@import_data.command()
@click.argument('file')
def import_college_data(file):
    inpf = openpyxl.load_workbook(file)
    sheet = inpf["Colleges"]
    for row in sheet.iter_rows(min_row=2, values_only=True):
        clg = College(name=row[0], location=row[2], acronym=row[1], contact=row[3])
        clg.save()


@import_data.command()
@click.argument('file')
def import_student_data(file):
    input_file = openpyxl.load_workbook(file)
    sheet = input_file["Current"]
    for row in sheet.iter_rows(min_row=2, values_only=True):
        clg = College.objects.get(acronym=row[1])
        student = Student(name=row[0], email=row[2], db_folder=row[3], college=clg, dropped_out=False)
        student.save()


@import_data.command()
@click.argument('file')
def import_mocktest1_data(file):
    input_file = openpyxl.load_workbook(file)
    sheet = input_file.active
    for row in sheet.iter_rows(min_row=2, values_only=True):
        try:
            student = Student.objects.get(name=row[0].split("_")[2])
        except Exception:
            continue
        test1 = Mocktest1(problem1=row[1], problem2=row[2], problem3=row[3], problem4=row[4], total=row[5], student=student)
        test1.save()


if __name__ == '__main__':
    objects = College.objects.all()
    import_data()
