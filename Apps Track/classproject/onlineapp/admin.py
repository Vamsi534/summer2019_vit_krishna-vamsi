from django.contrib import admin
from .models import College, Student, Mocktest1, Teacher

# Register your models here.
admin.site.register(College)
admin.site.register(Student)
admin.site.register(Mocktest1)
admin.site.register(Teacher)
