from django.shortcuts import render
from django.http import HttpResponse
from .models import College, Student, Mocktest1


# Create your views here.
def hello(req):
    return HttpResponse(req.headers['foo'])


def get_my_college(request):
    my_college = College.objects.all().get(acronym="VIT")
    return HttpResponse(my_college.contact)


def get_all_colleges(request):
    colleges = College.objects.values("id", "acronym", "name")
    return render(request, 'colleges.html', {'colleges': colleges})


def get_student_details(request, college_id):
    students = Student.objects.filter(college__id = id).values('id', 'name')
    return render(request, 'students.html', {'students': students})
