from django.db import models

# Create your models here.


class College(models.Model):
    name = models.CharField(max_length=50)
    location = models.CharField(max_length=50)
    acronym = models.CharField(max_length=10)
    contact = models.EmailField()

    def __str__(self):
        return self.acronym


class Student(models.Model):
    name = models.CharField(max_length=50)
    dob = models.DateField(null=True)
    email = models.EmailField()
    db_folder = models.CharField(max_length=50)
    college = models.ForeignKey(to=College, on_delete=models.CASCADE)
    dropped_out = models.BooleanField()

    def __str__(self):
        return self.name


class Mocktest1(models.Model):
    problem1 = models.IntegerField()
    problem2 = models.IntegerField()
    problem3 = models.IntegerField()
    problem4 = models.IntegerField()
    total = models.IntegerField()
    student = models.OneToOneField(Student, on_delete=models.CASCADE)

    def __str__(self):
        return f"Student{self.student.name} marks{self.total}"


class Teacher(models.Model):
    name = models.CharField(max_length=50)
    college = models.ForeignKey(College, on_delete=models.CASCADE)
