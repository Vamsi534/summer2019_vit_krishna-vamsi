from django.urls import path
from .views import *

urlpatterns = [
    path('get_my_college/', get_my_college),
    path('get_all_colleges/', get_all_colleges),
    path('get_student_details/<int:id>', get_student_details),
]