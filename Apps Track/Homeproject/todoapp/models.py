from django.db import models
import datetime

# Create your models here.
class Todolist(models.Model):
    name = models.CharField(max_length=20)
    created = models.DateField(auto_now_add=datetime.datetime.now())

    def __str__(self):
        return self.name


class Todoitem(models.Model):
    description = models.TextField(max_length=100)
    due_date = models.DateField(blank=True)
    completed = models.BooleanField()
    todolist = models.ForeignKey(Todolist, on_delete=models.CASCADE)
